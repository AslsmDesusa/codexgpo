'use strict'

var nodemailer = require("nodemailer");


const generateOTP = () =>{
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = ((Math.random() * 46656) | 0).toString(36);
    let secondPart = ((Math.random() * 46656) | 0).toString(36);
    firstPart = ("000" + firstPart).slice(-3);
    secondPart = ("000" + secondPart).slice(-3);
    return firstPart + secondPart;
}

// exports.send_mail = send_mail;
let send_mail = (to_email, subject, hostName, user_id) => {
    // create reusable transport method (opens pool of SMTP connections)
    
    var transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.USERNAME,
          pass: process.env.PASSWORD
        }
    });

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Codex Team " <no-reply>', // sender address
        to: to_email, // list of receivers
        subject: subject, // Subject line
        html: `<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"><tbody><tr><td align="center"><table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0"><tbody><tr><td align="center" valign="top" background="https://designmodo.com/demo/emailtemplate/images/header-background.jpg" bgcolor="#66809b" style="background-size:cover; background-position:top;height=" 400""=""><table class="col-600" width="600" height="400" border="0" align="center" cellpadding="0" cellspacing="0"><tbody><tr><td height="40"></td></tr<tr><td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px; margin: 30px;" src="https://designmodo.com/demo/emailtemplate/images/logo.png" width="109" height="103" alt="logo"></td></tr><tr><td align="center" style="font-family: 'Raleway', sans-serif; font-size:37px; color:#ffffff; line-height:24px; font-weight: bold; letter-spacing: 7px;">Codex <span style="font-family: 'Raleway', sans-serif; font-size:37px; color:#ffffff; line-height:39px; font-weight: 300; letter-spacing: 7px;">Platform</span></td></tr><tr><td height="50"></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td align="center"><table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px; border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;"><tbody><tr><td height="35"></td></tr><tr><td height="10"></td></tr><tr><td align="center" style="font-family: 'Lato', sans-serif; font-size:14px; color:#757575; line-height:24px; font-weight: 300; padding: 40px;">Codex Services has invited you to work on their Codex Partners dashboard. Create a Codexgpo account or log in to your existing one to accept the invitation..<br><div style="display: flex; justify-content: center !important;padding-left: 140px;"><a href="http://${hostName}/#/app/sessions/seeInvitation?user_id=${user_id}" target="_blank"><button type="submit" style="width: 100%;height: 5vh !important;cursor: pointer !important;border-radius: 50px; margin: 10px !important;background: rebeccapurple;color: white;">See Invitation</button></a></div></td></tr></tbody></table></td></tr><tr><td align="center"><table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9; "><tbody><tr><td height="10"></td></tr><tr><td></td></tr><tr><td height="30"></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td height="5"></td></tr>` // html body
    }

    // send mail with defined transport object
    return transporter.sendMail(mailOptions)
        .then((data)=> {
          return Promise.resolve({
            Message: `Invitation sent`,
          })
        })
        .catch(error => {
          throw error.message
        })
}

export default send_mail
