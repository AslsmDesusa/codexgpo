function OnSuccessFailure(status, statusCode, message, data) {
    return ({
        status: status,
        statusCode: statusCode,
        message: message,
        result: data
    })
}

module.exports = {OnSuccessFailure}