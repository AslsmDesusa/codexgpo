'use strict'

const Joi = require('joi')

let  JoiValidateDepartmentEquipment = () =>{
    // Schema Validation
    const newEquipmentBasicInfo = ({
        department: Joi.string(),
    })
    return newEquipmentBasicInfo
}

module.exports = {JoiValidateDepartmentEquipment}