'use strict'

const Joi = require('joi')

let  JoiValidateContacts = () =>{
    // Schema Validation
    const newContact = ({
        // personal Information
        type: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string().required().allow('', null),
        phone: Joi.string().required().allow('', null),
        designation: Joi.string().allow('', null),
        
        // address Info
        address: Joi.string().required().allow('', null),
        company: Joi.string().required().allow('', null),
        city: Joi.string().required().allow('', null),
        country: Joi.string().required().allow('', null),
        state: Joi.string().required().allow('', null),
        pinCode: Joi.string().required().allow('', null),
        remarks: Joi.string().required().allow('', null),
        tags: Joi.string().required().allow('', null),
    })
    return newContact
}

module.exports = {JoiValidateContacts}