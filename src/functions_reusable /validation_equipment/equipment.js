'use strict'

const Joi = require('joi')

let  JoiValidateEquipmentBasicInfo = () =>{
    // Schema Validation
    const newEquipmentBasicInfo = ({
        department: Joi.string(),
        equipment: Joi.string(),
        name: Joi.string().allow('', null),
        skuCode: Joi.string(),
        modelNo: Joi.string(),
        Type: Joi.string().allow('', null),
        manufecture: Joi.string().allow('', null),
        vendors: Joi.string().allow('', null),
        modelLaunchDate: Joi.string().allow('', null),
        endOfLifeDate: Joi.string().allow('', null),
        description: Joi.string().allow('', null),
    })
    return newEquipmentBasicInfo
}

let  JoiValidateEquipmentCostAndWarranty = () =>{
    // Schema Validation
    const newEquipmentCostWarranty = ({
        PriceInstallationWarranty: Joi.string(),
        AnnualMaintenanceContract: Joi.string(),
        AnnualAMCEscalation: Joi.string(),
        ComprensiveMaintenanceContract: Joi.string(),
        AnnualCMCEscalation: Joi.string(),
        OptionsSelected: Joi.array(),
    })
    return newEquipmentCostWarranty
}


module.exports = {JoiValidateEquipmentBasicInfo, JoiValidateEquipmentCostAndWarranty}


        // equipmentCost: Joi.string(),
        // installationCost: Joi.string(),
        // maintenanceCost: Joi.string(),
        // operatingCost: Joi.string(),
        // trainingCost: Joi.string(),
        // serviceCost: Joi.string(),
        // warranty: Joi.string()