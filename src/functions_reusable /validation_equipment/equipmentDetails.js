'use strict'

const Joi = require('joi')

let  JoiValidateEquipmentDetails = () =>{
    // Schema Validation
    const newEquipmentDetails = ({
        equipment: Joi.string().required(),
        facility: Joi.array(),
        department: Joi.array(),
        technicalFields: Joi.array(),
        ratingKeys: Joi.array(),
    })
    return newEquipmentDetails
}


module.exports = {JoiValidateEquipmentDetails}