'use strict'

const Joi = require('joi')

let  JoiValidateFaqs = () =>{
    // Schema Validation
    const newQuestion = ({
        Question: Joi.string(),
        Answer: Joi.string(),
        tags: Joi.array(),
    })
    return newQuestion
}


module.exports = {JoiValidateFaqs}