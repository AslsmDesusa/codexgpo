'use strict'

const Joi = require('joi')

let  JoiValidateConsumable = () =>{
    // Schema Validation
    const newConsumable = ({
        // personal Information
        name: Joi.string().required(),
        cost: Joi.string().required(),
        description: Joi.string().allow('', null),
        tags: Joi.array()
    })
    return newConsumable
}

module.exports = {JoiValidateConsumable}