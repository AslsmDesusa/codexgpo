'use strict'

const Joi = require('joi')

let  JoiValidateHospitalDetails = () =>{
    // Schema Validation
    const newHospitalDetails = ({
        name: Joi.string().required(),
        address: Joi.string().allow('', null),      
        city: Joi.string().allow('', null),
        pinCode: Joi.string().required(), 
        state: Joi.string().allow('', null),
        speciality: Joi.string().allow('', null).empty(['', null]).default(''),
        category: Joi.string().allow('', null).empty(['', null]).default(''),
        totalNumberofBeds: Joi.string().required(),
        noOfICUBeds: Joi.string().required(),
        noOfOperationTheatre: Joi.string().required(),
        hospitalStatus: Joi.string().allow('', null).empty(['', null]).default(''),
        departments: Joi.array().allow('', null),
        typeOfHospital: Joi.string().allow('', null).empty(['', null]).default(''),  
        attachment: Joi.string().allow('', null),
        status: Joi.boolean().default(false)
    })
    return newHospitalDetails
}

module.exports = {JoiValidateHospitalDetails}