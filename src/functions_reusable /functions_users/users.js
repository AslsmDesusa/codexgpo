'use strict'

const Joi = require('joi')

let  JoiValidateUser = () =>{
    // Schema Validation
    const newUser = ({
        firstName: Joi.string(),
        lastName: Joi.string(),
        email: Joi.string(),
        password: Joi.string()
    })
    return newUser
}


module.exports = {JoiValidateUser}