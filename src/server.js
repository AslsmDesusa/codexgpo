// making it strict
'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package')

import userSignup from './routes/users/user_signup'
import userSignin from './routes/users/user_signin'

import faqs from './routes/faqs/faqs'

const Path = require('path');

// equipment
import equipmentBasicInfo from './routes/equipment/basicInfo'
import equipmentCostAndWarranty from './routes/equipment/costAndWarranty'
import equipmentInfo from './routes/equipment/info'
import relatedEquip from './routes/equipment/relatedEquipment'
import technicalDetails from './routes/equipment/technicalDetails'
import experts from './routes/equipment/expertsAndKOL'
import ConsumableSpares from './routes/equipment/ConsumableSpares'
import prominentHospital from './routes/equipment/addProminentHospital'
import addVarient from './routes/equipment/addVarients'
import addOnOptions from './routes/equipment/addOnOptions'

// Hospital 
import Hospital from './routes/hospitals/hospital_master'
import uploadFile from './routes/hospitals/uploadImage'

// technical Equipment
import equipment from './routes/technicalEquipment/equipment'
import uploadImage from './routes/equipment/uploadImageToS3'
import sitePrepUpload from './routes/technicalEquipment/uploadSitePrep'

// contact Info
import contactModule from './routes/contacts/contact_info'

// mailer
import mailer from './routes/mailer/invite_users'
import invitations from './routes/mailer/accepet_invitation'

// department equipment technical details
import department from './routes/department/department' 
// facility
import facility from './routes/facility/facility_master'

import consumable from './routes/consumable/consumable'

import rattings from './routes/equipment/rattings'

const db = require('../database').db

const port = process.env.PORT || 3000
const { NODE_ENV } = process.env

// console.log(process)

const init = async () => {

    const server = Hapi.server({
        port: port,
        routes: { cors: true }
    });

    const swaggerOptions = {
        info: {
                title: `Codexgpo v${Pack.version} API Documentation`,
                version: Pack.version,
            },
        };
 
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    if (process.env.NODE_ENV !== 'production') {

        // Static Folder
        await server.route({
            method: 'GET',
            path: '/{p*}',
            handler: {
                directory: {
                    path: Path.join(__dirname, 'dist')
                }
            }
        });

        // handle SPA
        await server.route({
            method: 'GET',
            path: '/.*/',
            handler: {
                file: './dist/index.html'
            }});
    }
    
    await server.route(userSignup);
    await server.route(userSignin)
    await server.route(equipmentBasicInfo)
    await server.route(equipmentCostAndWarranty)
    await server.route(equipmentInfo)
    await server.route(mailer)
    await server.route(invitations)
    await server.route(faqs)
    await server.route(relatedEquip)
    await server.route(contactModule)
    await server.route(technicalDetails)
    await server.route(Hospital)
    await server.route(facility)
    await server.route(uploadImage)
    await server.route(experts)
    await server.route(consumable)
    await server.route(ConsumableSpares)
    await server.route(rattings)
    await server.route(uploadFile)
    await server.route(prominentHospital)
    await server.route(addVarient)
    await server.route(addOnOptions)
    await server.route(sitePrepUpload)
    

    // department
    await server.route(department)

    // equipment
    await server.route(equipment)

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
