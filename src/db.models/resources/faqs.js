var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const FaqsSchema = new Schema({
    Question: String,
    Answer: String,
    Tags: Array,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'faqs' });


const Faqs = mongoose.model('Faqs', FaqsSchema)
module.exports = Faqs;