var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const DepartmentSchema = new Schema({
    department: String,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'departments' });


const Department = mongoose.model('Department', DepartmentSchema)
module.exports = Department;