var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const contactInfoSchema = new Schema({
    // personal Info
    type: {type: String, enum: ['Vendor', 'Codex Team', 'Manufacturer', 'Expert', 'Sales Rep'], default: 'Codex Team'},
    name: String,
    email: String,
    phone: String,
    designation: String,
    // address Info
    address: String,
    company: String,
    city: String,
    country: String,
    state: String,
    pinCode: String,
    remarks: String,
    tags: String,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'contacts' });


const Contact = mongoose.model('Contact', contactInfoSchema)
module.exports = Contact;