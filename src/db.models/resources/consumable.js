var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const consumableSchema = new Schema({
    // personal Info
    name: String,
    cost: String,
    description: String,
    tags: Array,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'consumables' });


const Consumable = mongoose.model('Consumable', consumableSchema)
module.exports = Consumable;