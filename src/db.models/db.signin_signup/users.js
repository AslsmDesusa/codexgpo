var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
require('dotenv').config()
const JWT = require('jsonwebtoken');

const Email = require('mongoose-type-mail');

const UserSchema = new Schema({
    userDetails:{
        firstName: String,
        lastName: String,
    },
    privateDetails:{
        email: {type:Email},
        password: String,
    },
    rols: {type: String, enum: ['Admin', 'User', 'Guest'], default: 'Guest'},
    invitation: {type: String, enum: ['Pending' ,'Accepted', 'AccounSwitched'], default: 'Pending'},
    // while signup
    updated: {type: Boolean, default: false},
    // end
    createAt:  {type: Date, default: Date.now},
}, { collection: 'users' });

UserSchema.statics.login = async function (email, password) {
    var query = { 'privateDetails.email' : email }
    const user = await this.findOne(query).exec();
    if (!user) return false;
    const doesMatch = await bcrypt.compare(password, user.privateDetails.password);
    return doesMatch ? user : false;
};

// generate token for user signup
UserSchema.statics.generateToken = function(id){
    const token = JWT.sign(id.toJSON(), process.env.API_SECRET_KEY);
    return token
}

UserSchema.pre('updateOne', async function(next) {
    const password = this.getUpdate().$set.privateDetails.password;
        if (!password) {
            return next();
        }
        try {
            const salt = bcrypt.genSaltSync();
            const hash = bcrypt.hashSync(password, salt);
            this.getUpdate().$set.privateDetails.password = hash;
            next();
        } catch (error) {
            return next(error);
        }
  });

// UserSchema.pre('findOneAndUpdate', function (next) {
//     var user = this;
//     console.log(user)

//     // only hash the password if it has been modified (or is new)
//     if (!user.isModified('privateDetails.password')) return next();

//     // generate a salt
//     bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
//         if (err) return next(err);

//         // hash the password along with our new salt
//         bcrypt.hash(user.privateDetails.password, salt, function (err, hash) {
//             if (err) return next(err);

//             // override the cleartext password with the hashed one
//             user.privateDetails.password = hash;
//             next();
//         });
//     });
// });

const User = mongoose.model('User', UserSchema)
module.exports = User;