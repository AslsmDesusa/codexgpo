var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const HospitalsSchema = new Schema({

    name: String,
    address: String,      
    city: String,
    pinCode: String, 
    state: String,
    speciality: {type: String, enum: ['Single', 'Multi Speciality', 'Super Speciality', ''], default: ''},
    category: {type: String, enum: ['Primary', 'Secondary', 'Tertiary', ''], default: ''}, 
    totalNumberofBeds: String,
    noOfICUBeds: String,
    noOfOperationTheatre: String,
    hospitalStatus: {type: String, enum: ['Working Hospital', 'Greenfield Project', ''], default: ''},
    departments: Array,
    typeOfHospital: {type: String, enum: ['Sole Proprietorship', 'Partnership', 'Private Limited', 'Trust Hospital', ''], default: ''},  
    attachment: String,
    // is Prominent
    status: {type: Boolean, default: false},
    // created date
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'hospitals' });


const hospital = mongoose.model('hospital', HospitalsSchema)
module.exports = hospital;