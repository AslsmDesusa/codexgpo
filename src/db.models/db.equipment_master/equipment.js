var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const EquipmentMasterSchema = new Schema({
    basicInfo: {
        department: String,
        equipment: String,
        name: String,
        skuCode: String,
        modelNo: String,
        Type: String,
        manufecture: String,
        vendors: String,
        endOfLifeDate: String,
        modelLaunchDate: String,
        description: String,
    },
    costAndWarranty:{
        // Equipment Price including installation and 1 year warranty
        PriceInstallationWarranty: {type: String, default: '0'},
        // Annual Maintenance Contract in %
        AnnualMaintenanceContract: {type: String, default: '0'},
        // Annual AMC Escalation in %
        AnnualAMCEscalation: {type: String, default: '0'},
        // Comprensive Maintenance Contract in %
        ComprensiveMaintenanceContract: {type: String, default: '0'},
        // Annual CMC Escalation in %
        AnnualCMCEscalation: {type: String, default: '0'},
        // Options Selected in array key and value
        OptionsSelected: {type: Array, default: []},

    },
    // costAndWarranty:{
    //     equipmentCost: {type: String, default: '0'},
    //     installationCost: {type: String, default: '0'},
    //     maintenanceCost: {type: String, default: '0'},
    //     operatingCost: {type: String, default: '0'},
    //     trainingCost: {type: String, default: '0'},
    //     serviceCost: {type: String, default: '0'},
    //     warranty: {type: String, default: '0'}
    // },
    // faqs related to this equipment
    faqs: Array,
    rattings: Array,
    experts: Array,
    consumables: Array,
    varientsOptions: Array,
    varientsCombinations: Array,
    addOnOptions: Array,
    sitePrepRequirement: Array,
    statuteryRequirement: Array,
    relatedEquipment: Array,
    technicalDetails: Array,
    prominentHospitals: Array,
    equipmentImg: {type: String, default: null},
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'equipmentMaster' });


const EquipmentMaster = mongoose.model('EquipmentMaster', EquipmentMasterSchema)
module.exports = EquipmentMaster;