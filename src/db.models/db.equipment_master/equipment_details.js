var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const EquipmentDetailsSchema = new Schema({
    equipment: String,
    facility: Array,
    department: Array,
    technicalFields: Array,
    ratingKeys: Array,
    sitePrepRequirement: Array,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'equipmentDetails' });


const EquipmentDetails = mongoose.model('EquipmentDetails', EquipmentDetailsSchema)
module.exports = EquipmentDetails;