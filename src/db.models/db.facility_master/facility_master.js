var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var Schema = mongoose.Schema;

const facilitySchema = new Schema({
    facility: String,
    createdAt:  {type: Date, default: Date.now}
}, { collection: 'facilitys' });


const facility = mongoose.model('facility', facilitySchema)
module.exports = facility;