const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/prominent-hospital',
        options: {
        // include this route in swagger documentation
            description: 'adding prominentHospitals Details',
            notes:"adding prominentHospitals  details",
            tags:['api'],
            validate:{
                payload: {
                    prominentHospitals: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {prominentHospitals: {$each: request.payload.prominentHospitals}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Porminent Hospital for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/prominent-hospital',
        options: {
        // include this route in swagger documentation
            description: 'pulling prominentHospitals details',
            notes:"pulling prominentHospitals details",
            tags:['api'],
            validate:{
                payload: {
                    prominentHospitals: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {prominentHospitals: {$in: request.payload.prominentHospitals}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'This consumable removed successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;