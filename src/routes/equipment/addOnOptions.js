const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/add-on-options',
        options: {
        // include this route in swagger documentation
            description: 'add On options Details',
            notes:"add On options details",
            tags:['api'],
            validate:{
                payload: {
                    addOnOptions: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {addOnOptions: {$each: request.payload.addOnOptions}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'option added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/add-on-options',
        options: {
        // include this route in swagger documentation
            description: 'pulling add on options details',
            notes:"pulling add on options details",
            tags:['api'],
            validate:{
                payload: {
                    addOnOptions: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {addOnOptions: {$in: request.payload.addOnOptions}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'This option removed successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;