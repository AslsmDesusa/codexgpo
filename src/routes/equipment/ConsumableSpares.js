const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/consumable-spares',
        options: {
        // include this route in swagger documentation
            description: 'adding consumable-spares details',
            notes:"adding consumable-spares details",
            tags:['api'],
            validate:{
                payload: {
                    consumables: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {consumables: {$each: request.payload.consumables}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'consumable for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/consumable-spares',
        options: {
        // include this route in swagger documentation
            description: 'pulling consumable-spares details',
            notes:"pulling consumable-spares details",
            tags:['api'],
            validate:{
                payload: {
                    consumables: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {consumables: {$in: request.payload.consumables}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'This consumable removed successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;