const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'GET',
        path: '/api/equipment-basic-info',
        options: {
        // include this route in swagger documentation
            description: 'getting all info from equipment table',
            notes:"getting info from equipment",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.find({basicInfo: Object}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'Equipment Data', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/equipment-basic-info/{_id}',
        options: {
        // include this route in swagger documentation
            description: 'getting info from equipment table by id',
            notes:"getting info from equipment by id",
            tags:['api'],
            validate:{
                params:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.find({_id: ObjectID(request.params._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'Equipment Data', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/equipment-other-models',
        options: {
        // include this route in swagger documentation
            description: 'getting all info from equipment table',
            notes:"getting info from equipment",
            tags:['api'],
            validate:{
                query:{
                    equipment: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.find({'basicInfo.equipment': request.query.equipment}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'data successfully fetched from database', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    }
]

export default routes;