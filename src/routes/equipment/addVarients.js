const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/add-varients-keys-value',
        options: {
        // include this route in swagger documentation
            description: 'adding add-varients-keys-value Details',
            notes:"adding add-varients-keys-value  details",
            tags:['api'],
            validate:{
                payload: {
                    varientsOptions: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {varientsOptions :request.payload.varientsOptions}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'varients for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/add-varients-combinations',
        options: {
        // include this route in swagger documentation
            description: 'adding add-varients-combinations Details',
            notes:"adding add-varients-combinations  details",
            tags:['api'],
            validate:{
                payload: {
                    varientsCombinations: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {varientsCombinations:request.payload.varientsCombinations}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'varients for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    // {
    //     method: 'POST',
    //     path: '/api/prominent-hospital',
    //     options: {
    //     // include this route in swagger documentation
    //         description: 'pulling prominentHospitals details',
    //         notes:"pulling prominentHospitals details",
    //         tags:['api'],
    //         validate:{
    //             payload: {
    //                 prominentHospitals: Joi.array().required()
    //             },
    //             query: {
    //                 _id: Joi.string().required()
    //             }
    //         }
    //     },
    //     handler: async(request, h)=>{
    //         return new Promise((resolve, reject)=>{
    //             equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {prominentHospitals: {$in: request.payload.prominentHospitals}}}).then(result=>{
    //                 resolve(OnSuccessFailure('success', 200, 'This consumable removed successfully', result))
    //             })
    //             .catch(error=>{
    //                 resolve(OnSuccessFailure('error', 404, error.message, ''))
    //             })
    //         })
    //     }
    // },
]

export default routes;