const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/experts-and-kol',
        options: {
        // include this route in swagger documentation
            description: 'adding experts details',
            notes:"adding experts details",
            tags:['api'],
            validate:{
                payload: {
                    experts: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {experts: {$each: request.payload.experts}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'expert for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/experts-and-kol',
        options: {
        // include this route in swagger documentation
            description: 'adding equipment details with technical fields',
            notes:"adding equipemtn details",
            tags:['api'],
            validate:{
                payload: {
                    experts: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {experts: {$in: request.payload.experts}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'expert removed successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;