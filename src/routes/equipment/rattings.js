const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/rattings',
        options: {
        // include this route in swagger documentation
            description: 'adding rattings details',
            notes:"adding rattings details",
            tags:['api'],
            validate:{
                payload: {
                    rattings: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {rattings: request.payload.rattings}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'User ratting for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/rattings',
        options: {
        // include this route in swagger documentation
            description: 'pulling rattings details',
            notes:"pulling rattings details",
            tags:['api'],
            validate:{
                payload: {
                    reviewedBy: Joi.string().required(),
                    description: Joi.string().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {rattings: {'reviewedBy': request.payload.reviewedBy, description: request.payload.description}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'User ratting removed successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;