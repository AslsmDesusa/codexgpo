import {JoiValidateEquipmentBasicInfo} from '../../functions_reusable /validation_equipment/equipment'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

// upload Image
import { uploadFileToS3 }  from "../../utils/uploadToS3"
import { DeleteAwsImage }  from "../../utils/uploadToS3"

const equipmentModal = require('../../db.models/db.equipment_master/equipment')

// match object id
const ObjectID = require('mongodb').ObjectID

// Joi validation
const Joi = require('joi');


const routes = [
    {
        method: 'POST',
        path: '/api/duplicate/document',
        options: {
        // include this route in swagger documentation
            description: 'duplicate equipment and change sku code and model number',
            notes:"duplicate equipment",
            tags:['api'],
            validate:{
                payload: {
                    skuCode: Joi.string().required(),
                    modelNo: Joi.string().required()
                },
                query: {
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOne({$or: [{'basicInfo.skuCode': request.payload.skuCode}, {'basicInfo.modelNo': request.payload.modelNo}]}).then(res=>{
                    if (res) {
                        if (res.basicInfo.skuCode == request.payload.skuCode) {
                            resolve(OnSuccessFailure('error', 409, 'This Sku Code already exist please try with another Sku Code', res))
                        }else {
                            resolve(OnSuccessFailure('error', 409, 'This Model Number already exist please try with another Model No', res))
                        }
                    } else {
                        equipmentModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                            if (res) {
                                delete res._id;
                                res.basicInfo.skuCode = request.payload.skuCode 
                                res.basicInfo.modelNo = request.payload.modelNo

                                const data =  {
                                    basicInfo: res.basicInfo,
                                    costAndWarranty: res.costAndWarranty,
                                    faqs: res.faqs,
                                    rattings: res.rattings,
                                    experts: res.experts,
                                    consumables: res.consumables,
                                    varientsOptions: res.varientsOptions,
                                    varientsCombinations: res.varientsCombinations,
                                    addOnOptions: res.addOnOptions,
                                    sitePrepRequirement: res.sitePrepRequirement,
                                    statuteryRequirement: res.statuteryRequirement,
                                    relatedEquipment: res.relatedEquipment,
                                    technicalDetails: res.technicalDetails,
                                    prominentHospitals: res.prominentHospitals,
                                    equipmentImg: res.equipmentImg,
                                }

                                let duplicateEquipment = new equipmentModal(data)

                                duplicateEquipment.save().then(res=>{
                                    resolve(OnSuccessFailure('success', 200, 'This duplicate block is created', res))
                                })
                                .catch(error=>{
                                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                                })
                            }
                        })
                    }
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/equipment-basic-info',
        options: {
        // include this route in swagger documentation
            description: 'store equipment basic information',
            notes:"store equipment details with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateEquipmentBasicInfo()
            }
        },
        handler: async(request, h)=>{
            let equipmentBasicInfo = new equipmentModal({basicInfo: request.payload})
            return new Promise((resolve, reject)=>{
                equipmentModal.findOne({$or: [{'basicInfo.skuCode': request.payload.skuCode}, {'basicInfo.modelNo': request.payload.modelNo}]}).then(res=>{
                    if (res) {
                        if (res.basicInfo.skuCode == request.payload.skuCode) {
                            resolve(OnSuccessFailure('error', 409, 'This Sku Code already exist please try with another Sku Code', res))
                        }else {
                            resolve(OnSuccessFailure('error', 409, 'This Model Number already exist please try with another Model No', res))
                        }
                    } else {
                        equipmentBasicInfo.save().then(res=>{
                            resolve(OnSuccessFailure('success', 200, 'Basic Info Data successfully stored', res))
                        })
                        .catch(error=>{
                            resolve(OnSuccessFailure('error', 404, error.message, ''))
                        })
                    }
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/equipment-basic-info',
        options: {
        // include this route in swagger documentation
            description: 'deleting department information',
            notes:"deleting department details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (res) {
                        if (res.equipmentImg) {
                            var imageLink = res.equipmentImg
                            let data = imageLink.split('/')
                            DeleteAwsImage(data[3], data[4])
                        }
                        equipmentModal.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                            resolve(OnSuccessFailure('success', 200, 'This equipment has been deleted', res))
                        })
                        .catch(error=>{
                            resolve(OnSuccessFailure('error', 404, error.message, ''))
                        })
                    }
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/equipment-basic-info',
        options: {
        // include this route in swagger documentation
            description: 'store equipment basic information',
            notes:"store equipment details with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateEquipmentBasicInfo(),
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            var EquipmentBasicInfo = { $set: 
                {
                    basicInfo: request.payload
                }
            }
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, EquipmentBasicInfo).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'Basic Info Data successfully stored', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    // unset Value
    {
        method: 'GET',
        path: '/api/unset-value',
        options: {
        // include this route in swagger documentation
            description: 'unset any infromation which not usable',
            notes:"unset infromation",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.updateMany({}, { $unset : { costAndWarranty : 1} }).then(res=>{
                    resolve(res)
                })
            })
        }
    },
]
export default routes;