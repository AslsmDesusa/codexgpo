const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/related-equipment',
        options: {
        // include this route in swagger documentation
            description: 'adding equipment details with technical fields',
            notes:"adding equipemtn details",
            tags:['api'],
            validate:{
                payload: {
                    relatedEquipment: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {relatedEquipment: {$each: request.payload.relatedEquipment}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Related Equipment Added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/related-equipment',
        options: {
        // include this route in swagger documentation
            description: 'adding equipment details with technical fields',
            notes:"adding equipemtn details",
            tags:['api'],
            validate:{
                payload: {
                    relatedEquipment: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {relatedEquipment: {$in: request.payload.relatedEquipment}}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Related Equipment Added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;