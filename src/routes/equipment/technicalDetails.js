const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/technical-details-of-equipment',
        options: {
        // include this route in swagger documentation
            description: 'adding technical details with technical fields',
            notes:"adding technical details",
            tags:['api'],
            validate:{
                payload: {
                    technicalDetails: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                var technicalData = { $set: 
                    {
                        technicalDetails: request.payload.technicalDetails
                    }
                }
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, technicalData).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Technical Details Saved successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    }
]

export default routes;