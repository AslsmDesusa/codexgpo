import {JoiValidateEquipmentCostAndWarranty} from '../../functions_reusable /validation_equipment/equipment'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const equipmentModal = require('../../db.models/db.equipment_master/equipment')

const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/equipment-cost-and-warranty/{_id}',
        options: {
        // include this route in swagger documentation
            description: 'updating equipment cost and warranty information',
            notes:"updating equipment details with some validation",
            tags:['api'],
            validate:{
                params: {
                    _id: Joi.string().required(),
                },
                payload: JoiValidateEquipmentCostAndWarranty()
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                var EquipmentCostAndWarranty = { $set: 
                    {
                        costAndWarranty: request.payload
                    }
                }
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.params._id)}, EquipmentCostAndWarranty)
                .then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'cost and warranty added successfully', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]
export default routes;