import { uploadFileToS3 }  from "../../utils/uploadToS3"
import { DeleteAwsImage }  from "../../utils/uploadToS3"
const equipmentModal = require('../../db.models/db.equipment_master/equipment')

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID
const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/upload-product/image',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Posting equipment Image with there data",
            notes:"Posting equipment with data Upoload to s3 bucket",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            },
            validate:{
                query:{
                    fileExt: Joi.string().required(),
                    _id: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var file = request.payload.file
                equipmentModal.findOne({_id: request.query._id}).then(res=>{
                    if (res) {
                        if (res.equipmentImg) {
                            var imageLink = res.equipmentImg
                            let data = imageLink.split('/')
                            DeleteAwsImage(data[3], data[4])
                        }
                        uploadFileToS3(file, request.query.fileExt, "equipmentImage", "image",  'codegpoo')
                        .then(({ fileLink })=>{
                            var image = { $set: 
                                {
                                    equipmentImg: fileLink
                                }
                            }
                            equipmentModal.findOneAndUpdate({_id: request.query._id}, image).then(res=>{
                                resolve(OnSuccessFailure('success', 200, 'Image Uploaded Successfully', res))
                            })
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/site-prep-requirement/image',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"uploading Site Prep Requirement",
            notes:"uploading Site Prep Requirement",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            },
            validate:{
                query:{
                    folderName: Joi.string().required(),
                    fileExt: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var file = request.payload.file
                uploadFileToS3(file, request.query.fileExt, request.query.folderName, "image",  'codegpoo')
                .then(({ fileLink })=>{
                    resolve(fileLink)
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/site-prep-requirement',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"uploading Site Prep Requirement",
            notes:"uploading Site Prep Requirement",
            validate:{
                payload:{
                    sitePrepRequirement: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {sitePrepRequirement: request.payload.sitePrepRequirement}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Site Prep Requirement for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/site-prep-requirement/remove',
        options: {
        // include this route in swagger documentation
            description: 'removeing site-prep-req details',
            notes:"removeing site-prep-req details",
            tags:['api'],
            validate:{
                payload: {
                    displayName: Joi.string().required(),
                    fileLink: Joi.string().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {sitePrepRequirement: {'displayName': request.payload.displayName, fileLink: request.payload.fileLink}}}).then(result=>{
                    var imageLink = request.payload.fileLink
                    let data = imageLink.split('/')
                    DeleteAwsImage(data[3], data[4]).then(res=>{
                        resolve(OnSuccessFailure('success', 200, 'Site Prep Requirement for this equipment removed successfully', result))
                    })
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },

    // statuteryRequirement
    {
        method: 'POST',
        path: '/api/statutery-requirement',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"uploading Site statuteryRequirement",
            notes:"uploading Site statuteryRequirement",
            validate:{
                payload:{
                    statuteryRequirement: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {statuteryRequirement: request.payload.statuteryRequirement}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Statutery Requirement for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/statutery-requirement/remove',
        options: {
        // include this route in swagger documentation
            description: 'removeing statutery-requirement details',
            notes:"removeing statutery-requirement details",
            tags:['api'],
            validate:{
                payload: {
                    displayName: Joi.string().required(),
                    fileLink: Joi.string().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {statuteryRequirement: {'displayName': request.payload.displayName, fileLink: request.payload.fileLink}}}).then(result=>{
                    var imageLink = request.payload.fileLink
                    let data = imageLink.split('/')
                    DeleteAwsImage(data[3], data[4]).then(res=>{
                        resolve(OnSuccessFailure('success', 200, 'Statutery Requirement for this equipment removed successfully', result))
                    })
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]

export default routes;
