import {JoiValidateContacts} from '../../functions_reusable /validation_contact_list/contact_list'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

// upload Image
const contactModal = require('../../db.models/resources/contect_list')


const ObjectID = require('mongodb').ObjectID
const Joi = require('joi')


const routes = [
    {
        method: 'POST',
        path: '/api/contact-by-multiple_ids',
        options: {
        // include this route in swagger documentation
            description: 'getting contacts with multiple ids',
            notes:"getting contacts with multiple ids",
            tags:['api'],
            validate:{
                payload:{ 
                    _ids: Joi.array().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.find({_id: {$in: request.payload._ids}}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'Contact Fetched Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/contact-list',
        options: {
        // include this route in swagger documentation
            description: 'store contact information',
            notes:"store contact details with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateContacts()
            },
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let contactInfo = new contactModal(request.payload)
                contactInfo.save().then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'New contact saved successfully', res))
                    }else{
                        resolve(OnSuccessFailure('error', 404, 'Something Went Wrong', ''))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 401, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/contact-list',
        options: {
        // include this route in swagger documentation
            description: 'getting contacts information',
            notes:"getting contacts details",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.find().then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'Contacts fetched successfully', res))   
                    }else{
                        resolve(OnSuccessFailure('error', 404, 'Something Went Wrong', ''))
                    }
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/contact-list',
        options: {
        // include this route in swagger documentation
            description: 'deleting contact information',
            notes:"deleting contact details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This contact has been deleted', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/contact-list',
        options: {
        // include this route in swagger documentation
            description: 'updating contact information',
            notes:"updating contact details",
            tags:['api'],
            validate:{
                payload: JoiValidateContacts(),
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This contact has been updated successfully', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    // gettting contact list by there type
    {
        method: 'GET',
        path: '/api/contact-list-manufacturer',
        options: {
        // include this route in swagger documentation
            description: 'getting Manufacturer information',
            notes:"getting Manufacturer details",
            tags:['api'],
            validate:{
                query:{
                    type: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.find({type: request.query.type}).then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'Contacts fetched successfully', res))   
                    }else{
                        resolve(OnSuccessFailure('error', 404, 'Something Went Wrong', ''))
                    }
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/contact-list-make',
        options: {
        // include this route in swagger documentation
            description: 'getting Manufacturer information',
            notes:"getting Manufacturer details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                contactModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'Contacts fetched successfully', res))   
                    }else{
                        resolve(OnSuccessFailure('error', 404, 'Something Went Wrong', ''))
                    }
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]
export default routes;