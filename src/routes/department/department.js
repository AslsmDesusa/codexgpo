import {JoiValidateDepartmentEquipment} from '../../functions_reusable /validation_department/department'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

// upload Image
const departmentModal = require('../../db.models/resources/department_equipment')


const ObjectID = require('mongodb').ObjectID
const Joi = require('joi')


const routes = [
    {
        method: 'POST',
        path: '/api/department',
        options: {
        // include this route in swagger documentation
            description: 'store equipment basic information',
            notes:"store equipment details with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateDepartmentEquipment()
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let departmenEquipment = new departmentModal(request.payload)
                departmenEquipment.department = departmenEquipment.department.trim()
                departmentModal.findOne({department: request.payload.department})
                .then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('error', 409, 'This department already exist', res))
                    } else {
                        departmenEquipment.save().then(result=>{
                            resolve(OnSuccessFailure('success', 200, 'New Department Added Successfully', result))
                        })
                        .catch(error=>{
                            resolve(OnSuccessFailure('error', 404, error.message, ''))
                        })
                    }
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/department',
        options: {
        // include this route in swagger documentation
            description: 'getting department information',
            notes:"getting department details",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                departmentModal.find().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'Department and Equipment with techinal details', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/department',
        options: {
        // include this route in swagger documentation
            description: 'deleting department information',
            notes:"deleting department details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                departmentModal.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This department has been deleted', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/department',
        options: {
        // include this route in swagger documentation
            description: 'updating department information',
            notes:"updating department details",
            tags:['api'],
            validate:{
                payload: JoiValidateDepartmentEquipment(),
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                request.payload.department = request.payload.department.trim()
                departmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This department has been updated', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]
export default routes;