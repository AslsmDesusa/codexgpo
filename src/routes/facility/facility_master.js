
import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const facilityModal = require('../../db.models/db.facility_master/facility_master')

const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/add/facility',
        options: {
        // include this route in swagger documentation
            description: 'adding Facility Details in database with some validations',
            notes:"adding Facility Details in data with some validation",
            tags:['api'],
            validate:{
                payload: {
                    facility: Joi.string().required(),
                },
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let newFacility = new facilityModal(request.payload)
                newFacility.facility = newFacility.facility.trim()
                newFacility.save().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'New Facility has been saved successfully', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/add/facility',
        options: {
        // include this route in swagger documentation
            description: 'updating Facility Details in database with uniq id',
            notes:"updating Facility data",
            tags:['api'],
            validate:{
                payload: {
                    facility: Joi.string().required(),
                },
                query:{
                    _id: Joi.string().required()
                }
            },
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                request.payload.facility = request.payload.facility.trim()
                facilityModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This Facility has been updated successfully', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/add/facility',
        options: {
        // include this route in swagger documentation
            description: 'gettig Facility DATA from database',
            notes:"getting Facility Data from database",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                facilityModal.find().then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'Facility data got successfully', res))
                    }else{
                        resolve(OnSuccessFailure('error', 401, 'Something Went Wrong', res))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/add/facility',
        options: {
        // include this route in swagger documentation
            description: 'deleting Facility DATA from database',
            notes:"deleting Facility Data from database with there _id (Uinq)",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                facilityModal.findOneAndDelete({_id: ObjectID(request.query._id)}).then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'This Facility data has been deleted successfully', res))
                    }else{
                        resolve(OnSuccessFailure('error', 401, 'Something Went Wrong', res))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
]
export default routes;