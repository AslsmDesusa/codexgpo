import { uploadFileToS3 }  from "../../utils/uploadToS3"
import { DeleteAwsImage }  from "../../utils/uploadToS3"

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const hospitalModal = require('../../db.models/db.hospital_master/hospitalMaster')

const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/upload-file/hospital',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"Posting file in hospital master",
            notes:"Posting file in hospital master",
            payload:{
                maxBytes: 1000 * 1000 * 5, // 5 Mb
            },
            validate:{
                query:{
                    folderName: Joi.string().required(),
                    fileExt: Joi.string().required(),
                    _id: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                var file = request.payload.file
                hospitalModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (res) {
                        if (res.attachment) {
                            var imageLink = res.attachment
                            let data = imageLink.split('/')
                            DeleteAwsImage(data[3], data[4])
                        }
                        uploadFileToS3(file, request.query.fileExt, request.query.folderName, "image",  'codegpoo')
                        .then(({ fileLink })=>{
                            var image = { $set: 
                                {
                                    attachment: fileLink
                                }
                            }
                            hospitalModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, image).then(res=>{
                                resolve(OnSuccessFailure('success', 200, 'File Uploaded Successfully', fileLink))
                            }).catch(err=>{
                                resolve(OnSuccessFailure('error', 400, err.message, ''))
                            })
                        })
                    }
                })
            }
            return new Promise(pr)
        }  
    },
]
export default routes;