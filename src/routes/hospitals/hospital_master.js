
import {OnSuccessFailure} from '../../http_list_status/status_messages'
import {JoiValidateHospitalDetails} from '../../functions_reusable /validation_hospital/hospital_master'

const ObjectID = require('mongodb').ObjectID

import { DeleteAwsImage }  from "../../utils/uploadToS3"

const hospitalModal = require('../../db.models/db.hospital_master/hospitalMaster')

const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/add/hospital-details',
        options: {
        // include this route in swagger documentation
            description: 'adding Hospital Details in database with some validations',
            notes:"adding Hospital Details in data with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateHospitalDetails(),
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let newHospitalData = new hospitalModal(request.payload)
                newHospitalData.save().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'New Hospital has been saved successfully', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/add/hospital-details',
        options: {
        // include this route in swagger documentation
            description: 'updating Hospital Details in database with uniq id',
            notes:"updating hospital data",
            tags:['api'],
            validate:{
                payload: JoiValidateHospitalDetails(),
                query:{
                    _id: Joi.string().required()
                }
            },
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                hospitalModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This Hospital has been updated successfully', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/add/hospital-details',
        options: {
        // include this route in swagger documentation
            description: 'gettig Hospital DATA from database',
            notes:"getting hospital Data from database",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                hospitalModal.find().then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'hospital data got successfully', res))
                    }else{
                        resolve(OnSuccessFailure('error', 401, 'Something Went Wrong', res))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/add/hospital-details/prominent',
        options: {
        // include this route in swagger documentation
            description: 'gettig Hospital DATA from database with prominent keyword',
            notes:"getting hospital Data from database with prominent keyword",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                hospitalModal.find({status: true}).then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('success', 200, 'hospital data got successfully', res))
                    }else{
                        resolve(OnSuccessFailure('error', 401, 'Something Went Wrong', res))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 400, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/add/hospital-details/prominent/by/multiple-ids',
        options: {
        // include this route in swagger documentation
            description: 'getting hospitals form database with answer and tags',
            notes:"getting hospitals form database with answer and tags",
            tags:['api'],
            validate:{
                payload:{ 
                    _ids: Joi.array().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                hospitalModal.find({_id: {$in: request.payload._ids}}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'hospitals Fetched Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/add/hospital-details',
        options: {
        // include this route in swagger documentation
            description: 'deleting Hospital DATA from database',
            notes:"deleting hospital Data from database with there _id (Uinq)",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                hospitalModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (res) {
                        if (res.attachment) {
                            var imageLink = res.attachment
                            let data = imageLink.split('/')
                            DeleteAwsImage(data[3], data[4])
                        }
                        hospitalModal.findOneAndDelete({_id: ObjectID(request.query._id)}).then(res=>{
                            if (res) {
                                resolve(OnSuccessFailure('success', 200, 'This Hospital data has been deleted successfully', res))
                            }else{
                                resolve(OnSuccessFailure('error', 401, 'Something Went Wrong', res))
                            }
                        }).catch(err=>{
                            resolve(OnSuccessFailure('error', 400, err.message, ''))
                        })
                    }
                })
            })
        }
    },
]
export default routes;