import {JoiValidateConsumable} from '../../functions_reusable /validation_consumable/consumable'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

// upload Image
const consumableModel = require('../../db.models/resources/consumable')


const ObjectID = require('mongodb').ObjectID
const Joi = require('joi')


const routes = [
    {
        method: 'POST',
        path: '/api/consumable-by-multiple_ids',
        options: {
        // include this route in swagger documentation
            description: 'getting consumable with multiple ids',
            notes:"getting consumable with multiple ids",
            tags:['api'],
            validate:{
                payload:{ 
                    _ids: Joi.array().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                consumableModel.find({_id: {$in: request.payload._ids}}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'consumables Fetched Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/consumable',
        options: {
        // include this route in swagger documentation
            description: 'store consumable information',
            notes:"store consumable with some validation",
            tags:['api'],
            validate:{
                payload: JoiValidateConsumable()
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let newConsumable = new consumableModel(request.payload)
                consumableModel.findOne({name: request.payload.name})
                .then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('error', 409, 'This equipment already exist', res))
                    } else {
                        newConsumable.save().then(result=>{
                            resolve(OnSuccessFailure('success', 200, 'New consumable Equipment Added Successfully', result))
                        })
                        .catch(error=>{
                            resolve(OnSuccessFailure('error', 404, error.message, ''))
                        })
                    }
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/consumable',
        options: {
        // include this route in swagger documentation
            description: 'getting department information',
            notes:"getting department details",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                consumableModel.find().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'consumable data fetched successfully', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/consumable',
        options: {
        // include this route in swagger documentation
            description: 'deleting department information',
            notes:"deleting department details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                consumableModel.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This consumable equipment has been deleted', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/consumable',
        options: {
        // include this route in swagger documentation
            description: 'updating department information',
            notes:"updating department details",
            tags:['api'],
            validate:{
                payload: JoiValidateConsumable(),
                query:{
                    _id: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                consumableModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This consumable equipment has been updated', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]
export default routes;