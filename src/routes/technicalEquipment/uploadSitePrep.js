import { uploadFileToS3 }  from "../../utils/uploadToS3"
import { DeleteAwsImage }  from "../../utils/uploadToS3"

import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const equipmentDetailsModel = require('../../db.models/db.equipment_master/equipment_details')

const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/site-prep-requirement/equipment-details',
        config: {
             //include this route in swagger documentation
            tags:['api'],
            description:"uploading Site Prep Requirement",
            notes:"uploading Site Prep Requirement",
            validate:{
                payload:{
                    sitePrepRequirement: Joi.array().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async (request, h) => {
            let pr = async (resolve, reject)=>{
                equipmentDetailsModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$push: {sitePrepRequirement: request.payload.sitePrepRequirement}}).then(result=>{
                    resolve(OnSuccessFailure('success', 200, 'Site Prep Requirement for this equipment added successfully', result))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            }
            return new Promise(pr)
        }  
    },
    {
        method: 'POST',
        path: '/api/site-prep-requirement/remove/equipment-details',
        options: {
        // include this route in swagger documentation
            description: 'removeing site-prep-req details',
            notes:"removeing site-prep-req details",
            tags:['api'],
            validate:{
                payload: {
                    displayName: Joi.string().required(),
                    fileLink: Joi.string().required()
                },
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentDetailsModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, {$pull: {sitePrepRequirement: {'displayName': request.payload.displayName, fileLink: request.payload.fileLink}}}).then(result=>{
                    var imageLink = request.payload.fileLink
                    let data = imageLink.split('/')
                    DeleteAwsImage(data[3], data[4]).then(res=>{
                        resolve(OnSuccessFailure('success', 200, 'Site Prep Requirement for this equipment removed successfully', result))
                    })
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
]
export default routes;