import {JoiValidateEquipmentDetails} from '../../functions_reusable /validation_equipment/equipmentDetails'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

// upload Image
const equipmentModal = require('../../db.models/db.equipment_master/equipment_details')

// match object id
const ObjectID = require('mongodb').ObjectID

// Joi validation
const Joi = require('joi')

const routes = [
    {
        method: 'POST',
        path: '/api/equipment',
        options: {
        // include this route in swagger documentation
            description: 'adding equipment details with technical fields',
            notes:"adding equipemtn details",
            tags:['api'],
            validate:{
                payload: JoiValidateEquipmentDetails()
            }
        }, 
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                Object.keys(request.payload).map(function(key, index) {
                    if (typeof(request.payload[key]) === 'string') {
                        request.payload[key] = request.payload[key].trim()
                    }
                });
                let departmenEquipment = new equipmentModal(request.payload)
                equipmentModal.findOne({equipment: request.payload.equipment})
                .then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('error', 409, 'This equipment already exist', res))
                    } else {
                        departmenEquipment.save().then(result=>{
                            resolve(OnSuccessFailure('success', 200, 'New Equipment Added Successfully', result))
                        })
                        .catch(error=>{
                            resolve(OnSuccessFailure('error', 404, error.message, ''))
                        })
                    }
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/equipment',
        options: {
        // include this route in swagger documentation
            description: 'getting equipment details with technical fields',
            notes:"getting equipment details",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.find().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'equipment details', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/equipment/{department}',
        options: {
        // include this route in swagger documentation
            description: 'getting equipment details with technical fields',
            notes:"getting equipment details",
            tags:['api'],
            validate:{
                params:{
                    department: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.find({department: request.params.department}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'equipment details', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/equipment/{equipment}/technical-fields',
        options: {
        // include this route in swagger documentation
            description: 'getting equipment details with technical fields',
            notes:"getting equipment details",
            tags:['api'],
            validate:{
                params:{
                    equipment: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOne({equipment: request.params.equipment}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'equipment details fetched successfully', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/equipment',
        options: {
        // include this route in swagger documentation
            description: 'deleting equipment details with technical fields',
            notes:"deleting equipment details",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModal.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This equipment name has been deleted', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/equipment',
        options: {
            description: 'deleting equipment details with technical fields',
            notes:"deleting equipment details",
            tags:['api'],
            // include this route in swagger documentation
            validate:{
                payload: JoiValidateEquipmentDetails(),
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                Object.keys(request.payload).map(function(key, index) {
                    if (typeof(request.payload[key]) === 'string') {
                        request.payload[key] = request.payload[key].trim()
                    }
                });
                equipmentModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, request.payload).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This equipment name has been updated', res))
                })
                .catch(error=>{
                    resolve(OnSuccessFailure('error', 404, error.message, ''))
                })
            })
        }
    }
]
export default routes;