import {OnSuccessFailure} from '../../http_list_status/status_messages'
const userModal = require('../../db.models/db.signin_signup/users')

require('dotenv').config();
const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/user-signin',
        options: {
        // include this route in swagger documentation
            description: 'user signin details',
            notes:"user signin details with user email or passwrod",
            tags:['api'],
            validate:{
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                userModal.findOne({'privateDetails.email': request.payload.email}, async function(err, res){
                    if (err) {
                        resolve({"status": "error", "message": err})
                    }else if (!res) {
                        resolve(OnSuccessFailure('error', 204, 'There is no user record corresponding to this identifier. The user may have been deleted.', 'Unauthorized'))
                    } else {
                        const validUser = await userModal.login(request.payload.email, request.payload.password);
                        if (validUser) {
                            const token = userModal.generateToken(res)
                            resolve(OnSuccessFailure('success', 200, 'Successfully Logged In', token))
                        }else{
                            resolve(OnSuccessFailure('error', 401,'The password is invalid or the user does not have a password.', 'Unauthorized'))
                        }
                    }
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/user-signin/by/google',
        options: {
        // include this route in swagger documentation
            description: 'user signin details',
            notes:"user signin details with user email or passwrod",
            tags:['api'],
            validate:{
                payload: {
                    email: Joi.string().required(),
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                userModal.findOne({'privateDetails.email': request.payload.email}, async function(err, res){
                    if (err) {
                        resolve({"status": "error", "message": err})
                    }else if (!res) {
                        resolve(OnSuccessFailure('error', 204, 'There is no user record corresponding to this identifier. The user may have been deleted.', 'Unauthorized'))
                    } else {
                        resolve(OnSuccessFailure('success', 200, 'Successfully Logged In', res))
                    }
                })
            })
        }
    },
]
export default routes;




// {
//     options: {
//         size: [
//             {
//                 optionSize: L
//             },
//             {
//                 optionSize: XL
//             },
//             {
//                 optionSize: XXL
//             },
//         ],
//         material: [
//             {
//                 materialSize: 'Plastic'
//             },
//             {
//                 materialSize: 'Fiber'
//             }
//         ]
//     }
//     optionData:{
//         L-Plastic: ''
//         XL-Plastic: ''
//         XXL-Plastic: ''
//         L-Fiber: ''
//         XL-Fiber: ''
//         XXL-Fiber: ''
//     }
// }