import { JoiValidateUser } from '../../functions_reusable /functions_users/users'
import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const Joi = require('joi');

const userModal = require('../../db.models/db.signin_signup/users')

const routes = [
    {
        method: 'POST',
        path: '/api/user-signup-info',
        options: {
        // include this route in swagger documentation
            description: 'user signup details',
            notes:"user signup details with user email or passwrod",
            tags:['api'],
            validate:{
                payload: JoiValidateUser(),
                query: {
                    user_id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            var userSignup = { $set: 
                {
                    userDetails:{
                        firstName: request.payload.firstName,
                        lastName: request.payload.lastName,
                    },
                    privateDetails:{
                        email: request.payload.email,
                        password: request.payload.password,
                    },
                    updated: true,
                }
            };
            return new Promise((resolve, reject)=>{
                userModal.findOne({_id: ObjectID(request.query.user_id)}).then(res=>{
                    if (!res) {
                        resolve(OnSuccessFailure('error', 409, 'This invitation may have been deleted by Admin', ''))
                    }else{
                        if (res.privateDetails.email == request.payload.email) {
                            userModal.updateOne({_id: ObjectID(request.query.user_id)}, userSignup)
                            .then(res=>{
                                resolve(OnSuccessFailure('success', 200, 'Your account has been successfully activated', res))
                            })
                            .catch(error=>{
                                resolve(OnSuccessFailure('error', 404, error.message, ''))
                            })
                        } else {
                            resolve(OnSuccessFailure('error', 300, 'Please use the same email from which you have been invited.', ''))
                        }
                    }
                })
            })
        }
    },
]
export default routes;