
import {OnSuccessFailure} from '../../http_list_status/status_messages'
import {JoiValidateFaqs} from '../../functions_reusable /functions_faqs/faqs'

const ObjectID = require('mongodb').ObjectID

const faqsModels = require('../../db.models/resources/faqs')
// for adding faqs
const equipmentModels = require('../../db.models/db.equipment_master/equipment')


const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/add/faqs',
        options: {
        // include this route in swagger documentation
            description: 'adding FAQs in database with answer and tags',
            notes:"adding FAQs in database with answer and tags",
            tags:['api'],
            validate:{
                payload: {
                    faqs: Joi.array().required()
                },
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModels.findOneAndUpdate({_id: request.query._id}, {$push: {faqs: {$each: request.payload.faqs}}}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'New Question Added successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/delete/faqs',
        options: {
        // include this route in swagger documentation
            description: 'deleteing FAQs in database with answer and tags',
            notes:"deleteing FAQs in database with answer and tags",
            tags:['api'],
            validate:{
                payload: {
                    faqs: Joi.array().required()
                },
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModels.findOneAndUpdate({_id: request.query._id}, { $pull: { 'faqs': {$in: request.payload.faqs}} }).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This Faqs remove successfully from this Equipment', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'PUT',
        path: '/api/update/faqs',
        options: {
        // include this route in swagger documentation
            description: 'updateing FAQs for equipment deshboard',
            notes:"updateing FAQs for equipment deshboard",
            tags:['api'],
            validate:{
                payload: {
                    Question: Joi.string().required(),
                    Answer: Joi.string().required()
                },
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                equipmentModels.findOneAndUpdate({_id: ObjectID(request.query._id), 'faqs.Question': request.payload.Question}, 
                {
                    $set :
                    {
                        "faqs.$.Answer": request.payload.Answer
                    }
                }).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This Question updated successfully', res))
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/faqs',
        options: {
        // include this route in swagger documentation
            description: 'posting FAQs in database with answer and tags',
            notes:"posting FAQs in database with answer and tags",
            tags:['api'],
            validate:{
                payload: JoiValidateFaqs()
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                let newFaqs = new faqsModels(request.payload)
                newFaqs.save().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'New Question save successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/faqs',
        options: {
        // include this route in swagger documentation
            description: 'getting FAQs in database with answer and tags',
            notes:"getting FAQs in database with answer and tags",
            tags:['api'],
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                faqsModels.find().then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'FAQs Fetched Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'POST',
        path: '/api/faqss',
        options: {
        // include this route in swagger documentation
            description: 'getting FAQs form database with answer and tags',
            notes:"getting FAQs form database with answer and tags",
            tags:['api'],
            validate:{
                payload:{ 
                    _ids: Joi.array().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                faqsModels.find({_id: {$in: request.payload._ids}}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'FAQs Fetched Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/faqs',
        options: {
        // include this route in swagger documentation
            description: 'deleting FAQs in database with answer and tags',
            notes:"deleting FAQs in database with answer and tags",
            tags:['api'],
            validate:{
                query:{
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                faqsModels.findOneAndRemove({_id: ObjectID(request.query._id)}).then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'This FAQs deleted Successfully', res))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
]
export default routes;