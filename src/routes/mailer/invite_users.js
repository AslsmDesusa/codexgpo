
import send_mail from '../../utils/mailer'

import {OnSuccessFailure} from '../../http_list_status/status_messages'
import { invalid } from 'joi';

const ObjectID = require('mongodb').ObjectID

const userModal = require('../../db.models/db.signin_signup/users')

const Joi = require('joi');

const routes = [
    {
        method: 'POST',
        path: '/api/invite-user',
        options: {
        // include this route in swagger documentation
            description: 'send invitation to new users',
            notes:"only admin can send invitaitons",
            tags:['api'],
            validate:{
                payload: {
                    email: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            request.payload.email = request.payload.email.trim()
            let InviteUser = new userModal({
                userDetails:{
                    firstName: '',
                    lastName: '',
                },
                privateDetails:{
                    email: request.payload.email,
                    password: '',
                },
                rols: 'Admin'
            })
            return new Promise((resolve, reject)=>{
                let inv = 'You’ve been invited to work on Codex Platform'
                userModal.findOne({'privateDetails.email': request.payload.email}).then(res=>{
                    if (res) {
                        resolve(OnSuccessFailure('error', 409, 'This email address already exist in our system please try with another email', ''))
                    } else {
                        send_mail(request.payload.email, inv, request.headers.host, InviteUser._id)
                        .then(({ Message, OTP }) =>{
                            InviteUser.save().then(res=>{
                                resolve(OnSuccessFailure('success', 200, `invitation sent to ${request.payload.email}`, res))
                            })
                            .catch(err=>{
                                resolve(err)
                            })
                        })
                        .catch(err=>{
                            resolve(OnSuccessFailure('error', 409, err, ''))
                        })
                    }
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/invite-user',
        options: {
        // include this route in swagger documentation
            description: 'get user by there rols',
            notes:"getting user by rols",
            tags:['api'],
            validate:{
                query: {
                    rols: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                userModal.find({rols: request.query.rols})
                .then(res=>{
                    resolve(res)
                })
            })
        }
    },
    {
        method: 'DELETE',
        path: '/api/invite-user',
        options: {
        // include this route in swagger documentation
            description: 'delete user by there Id',
            notes:"deleteing user by id",
            tags:['api'],
            validate:{
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                userModal.findByIdAndDelete({_id: ObjectID(request.query._id)})
                .then(res=>{
                    resolve(OnSuccessFailure('success', 200, 'User Deleted successfully', ''))
                })
                .catch(err=>{
                    resolve(OnSuccessFailure('error', 404, err.message, ''))
                })
            })
        }
    },
]
export default routes;