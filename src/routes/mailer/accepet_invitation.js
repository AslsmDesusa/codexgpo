
import {OnSuccessFailure} from '../../http_list_status/status_messages'

const ObjectID = require('mongodb').ObjectID

const userModal = require('../../db.models/db.signin_signup/users')

const Joi = require('joi');

const routes = [
    {
        method: 'PUT',
        path: '/api/accept-invite-user',
        options: {
        // include this route in swagger documentation
            description: 'accept invitation to new users',
            notes:"only admin can send invitaitons",
            tags:['api'],
            validate:{
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                var invitationAcceptence = { $set: 
                    {
                        invitation: 'Accepted'
                    }
                };
                userModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (!res) {
                        resolve(OnSuccessFailure('error', 404, 'This invitation may have been deleted by Admin', ''))
                    }else if (res.invitation == 'Accepted') {
                        resolve(OnSuccessFailure('error', 404, 'This invitation to work with Codex Services has already been accepted.', 'Invitation already accepted'))
                    }else{
                        userModal.findOneAndUpdate({_id: ObjectID(request.query._id)}, invitationAcceptence).then(res=>{
                            resolve(OnSuccessFailure('success', 201, 'Invitation accepted please fill this form to get access for Codex deshboard', res))
                        }).catch(err=>{
                            resolve(OnSuccessFailure('error', 500, err.message, ''))
                        })
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 403, err.message, ''))
                })
            })
        }
    },
    {
        method: 'GET',
        path: '/api/check-accept-invitation',
        options: {
        // include this route in swagger documentation
            description: 'accept invitation to new users',
            notes:"only admin can send invitaitons",
            tags:['api'],
            validate:{
                query: {
                    _id: Joi.string().required()
                }
            }
        },
        handler: async(request, h)=>{
            return new Promise((resolve, reject)=>{
                userModal.findOne({_id: ObjectID(request.query._id)}).then(res=>{
                    if (!res) {
                        resolve(OnSuccessFailure('error', 404, 'This invitation may have been deleted by Admin', ''))
                    }else if (res.invitation == 'Accepted') {
                        resolve(OnSuccessFailure('error', 404, 'This invitation to work with Codex Services has already been accepted.', 'Invitation already accepted'))
                    }else{
                        resolve(OnSuccessFailure('success', 200, 'Codex Services has invited you to work on their Codex Platform Dashboard', 'Invitation from Codex Services'))
                    }
                }).catch(err=>{
                    resolve(OnSuccessFailure('error', 403, err.message, ''))
                })
            })
        }
    },
]
export default routes;