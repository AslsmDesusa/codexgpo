var mongoose = require('mongoose');
require('dotenv').config()

var os = require('os');

const Boom = require('boom');
const dbName = process.env.MONGO_DBNAME,
	dbUser = process.env.MONGO_DB_USER,
	dbPassword = process.env.MONGO_DB_PASSWORD,
	dbHost = process.env.MONGO_DB_HOST,
	dbPort = process.env.MONGO_DB_PORT,
	dbUrl = process.env.OTHER_DB_URL;

const dbLocalName = process.env.MONGO_LOCAL_DBNAME,
	dbLocalUser = process.env.MONGO_LOCAL_DB_USER, 
	dbLocalPassword = process.env.MONGO_LOCAL_DB_PASSWORD,
	dbLocalHost = process.env.MONGO_LOCAL_DB_HOST,
	dbLocalPort = process.env.MONGO_LOCAL_DB_PORT

if (!dbName) {
	console.log("Db name is not provide in env file ")
	throw Boom.notFound("DB not found");
}
// myHeaders.append('Content-Type', 'image/jpeg');)

let mongoUrl = false;

// console.log(os)
if (!os.hostname().indexOf("local") > -1) {
	mongoUrl = "mongodb://"+ dbLocalUser +":"+ dbLocalPassword +"@"+dbLocalHost+":"+dbLocalPort+"/"+dbLocalName
} else {
	mongoUrl = "mongodb://"+ dbUser +":"+ dbPassword +"@"+dbHost+":"+dbPort+","+dbUrl
}

mongoose.connect(mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
	console.log(`Connection with database succeeded. ${mongoUrl}`);
});
exports.db = db;
